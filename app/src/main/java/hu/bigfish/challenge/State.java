package hu.bigfish.challenge;

enum State {
    STARTED, LOGGED_IN, UPLOADED, SUBMITTED
}
