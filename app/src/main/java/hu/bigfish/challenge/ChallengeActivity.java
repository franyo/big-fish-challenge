package hu.bigfish.challenge;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.IOException;

import javax.inject.Inject;

import hu.bigfish.challenge.api.Api;
import hu.bigfish.challenge.api.LoginDto;
import hu.bigfish.challenge.application.ChallengeApplication;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okio.BufferedSource;
import okio.Okio;
import retrofit2.Retrofit;

public class ChallengeActivity extends AppCompatActivity {

    private static final String SATE_KEY = "STATE";

    @Inject
    Retrofit retrofit;

    @Inject
    Gson gson;

    private static final String TAG = "ChallengeActivity";

    private Api api;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_challenge);

        ((ChallengeApplication) getApplication()).getApplicationComponent().inject(this);
        api = retrofit.create(Api.class);

        changeState(loadAppropriateState());
    }

    private State loadAppropriateState() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String savedState = preferences.getString(SATE_KEY, State.STARTED.name());
        return State.SUBMITTED.name().equals(savedState) ? State.SUBMITTED : State.STARTED;
    }

    private void saveState(State state) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(SATE_KEY, state.name());
        editor.apply();
    }

    private void changeState(State state) {
        saveState(state);
        switch (state) {
            case STARTED:
                showView(R.id.wait_tv);
                logIn();
                break;
            case LOGGED_IN:
                showView(R.id.for_tv);
                uploadSource();
                break;
            case UPLOADED:
                showView(R.id.it_tv);
                submit();
                break;
            case SUBMITTED:
                showView(R.id.done_tv);
                break;
            default:
                break;
        }
    }

    private void showView(int resourceId) {
        findViewById(resourceId).setVisibility(View.VISIBLE);
    }

    private void logIn() {
        LoginDto loginDto = new LoginDto(getString(R.string.email), getString(R.string.password));
        api.login(loginDto).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(user -> {
            Log.i(TAG, "logIn response:" + gson.toJson(user));
            changeState(State.LOGGED_IN);
        }, error -> {
            Log.e(TAG, "logIn", error);
            Toast.makeText(this, error.getMessage(), Toast.LENGTH_LONG).show();
        });
    }

    private void uploadSource() {
        BufferedSource bufferedSource = Okio.buffer(Okio.source(getResources().openRawResource(R.raw.franyo_challenge_source)));
        try {
            RequestBody requestBody = RequestBody.create(MediaType.parse("application/octet-stream"), bufferedSource.readByteArray());
            MultipartBody.Part part = MultipartBody.Part.createFormData("franyo_challenge_source", "franyo_challenge_source.zip", requestBody);

            api.uploadDocument(part).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(responseBody -> {
                Log.i(TAG, "uploadDocument:done");
                changeState(State.UPLOADED);
            }, error -> {
                Log.e(TAG, "uploadDocument", error);
                Toast.makeText(this, error.getMessage(), Toast.LENGTH_LONG).show();
            });
        } catch (IOException exception) {
            Log.e(TAG, "uploadDocument", exception);
            Toast.makeText(this, exception.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void submit() {
        api.fetchSubmitToken().flatMap(submitDto -> {
            Log.i(TAG, "submit:fetchToken response:" + gson.toJson(submitDto));
            return api.submit(submitDto);
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(responseBody -> {
            Log.i(TAG, "submit:done");
            changeState(State.SUBMITTED);
        }, error -> {
            Log.e(TAG, "submit", error);
            Toast.makeText(this, error.getMessage(), Toast.LENGTH_LONG).show();
        });
    }
}
