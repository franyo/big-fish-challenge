package hu.bigfish.challenge.application;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {

    private ChallengeApplication application;

    public ApplicationModule(ChallengeApplication application) {
        this.application = application;
    }

    @Provides
    @Singleton
    ChallengeApplication provideApplication() {
        return application;
    }
}
