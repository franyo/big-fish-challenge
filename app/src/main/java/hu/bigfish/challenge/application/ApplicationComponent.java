package hu.bigfish.challenge.application;

import javax.inject.Singleton;

import dagger.Component;
import hu.bigfish.challenge.ChallengeActivity;
import hu.bigfish.challenge.api.ApiModule;

@Singleton
@Component(modules = {ApplicationModule.class, ApiModule.class})
public interface ApplicationComponent {
    void inject(ChallengeActivity activity);
}
