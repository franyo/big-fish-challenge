package hu.bigfish.challenge.api;

public class DocumentDto {

    private String uuid;
    private Long size;
    private String name;
    private Long created;

    public DocumentDto(String uuid, Long size, String name, Long created) {
        this.uuid = uuid;
        this.size = size;
        this.name = name;
        this.created = created;
    }

    public String getUuid() {
        return uuid;
    }

    public Long getSize() {
        return size;
    }

    public String getName() {
        return name;
    }

    public Long getCreated() {
        return created;
    }
}
