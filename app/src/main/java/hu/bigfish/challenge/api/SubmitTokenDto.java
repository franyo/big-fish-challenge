package hu.bigfish.challenge.api;

public class SubmitTokenDto {

    private String uuid;

    public SubmitTokenDto(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }
}
