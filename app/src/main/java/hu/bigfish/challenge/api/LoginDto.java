package hu.bigfish.challenge.api;

public class LoginDto {

    private String email;
    private String pswd;

    public LoginDto(String email, String pswd) {
        this.email = email;
        this.pswd = pswd;
    }

    public String getEmail() {
        return email;
    }

    public String getPswd() {
        return pswd;
    }
}
