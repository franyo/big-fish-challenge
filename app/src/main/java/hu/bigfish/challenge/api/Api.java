package hu.bigfish.challenge.api;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface Api {

    @POST("me")
    Observable<UserDto> login(@Body LoginDto loginDto);

    @GET("doc")
    Observable<List<DocumentDto>> fetchDocuments();

    @Multipart
    @POST("doc")
    Observable<ResponseBody> uploadDocument(@Part MultipartBody.Part document);

    @GET("submit")
    Observable<SubmitTokenDto> fetchSubmitToken();

    @POST("submit")
    Observable<ResponseBody> submit(@Body SubmitTokenDto submitTokenDto);
}
