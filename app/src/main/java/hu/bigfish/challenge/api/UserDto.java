package hu.bigfish.challenge.api;

public class UserDto {

    private String uuid;
    private String email;
    private Long created;

    public UserDto(String uuid, String email, Long created) {
        this.uuid = uuid;
        this.email = email;
        this.created = created;
    }

    public String getUuid() {
        return uuid;
    }

    public String getEmail() {
        return email;
    }

    public Long getCreated() {
        return created;
    }
}
